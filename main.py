#!/usr/bin/env python
import os
import fnmatch
import unittest
import importlib
from time import clock

import webapp2
from google.appengine.api import memcache

from division import division
from model import DivisionResult, get_saved_result


class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write("""
            <!doctype html>
            <html><head></head><body>
            Division API<br />
            <a href="/divide/4/?dividend=10">10/4</a><br />
            <a href="/divide/3/?dividend=25">25/3</a><br />
            <a href="/divide/7/?dividend=111">111/7</a><br />
            <a href="/divide/0/?dividend=42">42/0</a><br />
            <a href="/tests/">unit tests</a><br />
            </body></html>
        """)

        for argument in self.request.arguments():
            value = self.request.get(argument)


class DivisionHandler(webapp2.RequestHandler):
    def get(self, divisor):
        dividend = self.request.get('dividend')

        cache_result = memcache.get('%s/%s' % (dividend, divisor))
        if cache_result is not None:
            code = int(cache_result[:3])
            json_result = cache_result[3:]
        else:
            saved_result = get_saved_result(divisor, dividend)
            if saved_result is not None:
                code = saved_result.code
                json_result = saved_result.json_result

            else:
                code, json_result = division(divisor, dividend)
                result = DivisionResult(divisor=divisor,
                                        dividend=dividend,
                                        code=code,
                                        json_result=json_result)
                result.put()

            memcache.add('%s/%s' % (dividend, divisor),
                         str(code) + json_result, 10)

        self.response.headers['Content-Type'] = 'application/json'
        self.response.status = code
        self.response.write(json_result)


class UnitTestsHandler(webapp2.RequestHandler):
    """Based on http://ahoj.io/unit-testing-gae-apps-in-python"""

    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'

        suite = unittest.TestSuite()
        loader = unittest.TestLoader()
        test_cases = self._find_test_cases()

        for test_case in test_cases:
            suite.addTests(loader.loadTestsFromTestCase(test_case))

        startTime = clock()
        result = unittest.TextTestRunner(verbosity=2).run(suite)
        stopTime = clock()

        self.response.out.write(('Test cases (%d):\n' % len(test_cases)) +
                                '\n'.join(map(repr, test_cases)) + '\n\n')
        self._print_test_results_group(result, 'errors')
        self._print_test_results_group(result, 'failures')

        self.response.out.write('Total tests: %d\n' % result.testsRun)
        self.response.out.write('Status: %s (%s ms)\n' % ('OK' if result.wasSuccessful() else 'FAILED', (stopTime - startTime) * 1000))

    def _find_test_cases(self):
        test_cases = []
        for root, dirnames, filenames in os.walk('.'):
            for filename in fnmatch.filter(filenames, 'test_*.py'):
                class_path = os.path.splitext(
                    os.path.join(root, filename)[2:])[0].replace('/', '.')
                class_name = os.path.splitext(filename)[0] # 'test_foo_bar'
                class_name = class_name.split('_')[1:]     # ['foo', 'bar']
                class_name = (''.join(s[0].upper() + s[1:] for s in class_name)         + 'TestCase')                 # FooBarTestCase
                test_cases.append(
                    getattr(importlib.import_module(class_path), class_name))

        return test_cases

    def _print_test_results_group(self, result, name):
        list = getattr(result, name)
        if len(list):
            self.response.out.write(
                "%s (%d):\n" % (name.capitalize(), len(list)))
            for item in list:
                self.response.out.write('%s\n' % item[0])
            self.response.out.write('\n')


app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/divide/(\d+)/', DivisionHandler),
    ('/tests/', UnitTestsHandler)
], debug=True)
