import unittest
import json
import random

from division import division


class DivisionTestCase(unittest.TestCase):

    def test_valid_input(self):
        small_numbers = random.randint(1, 10), random.randint(1, 100)
        medium_numbers = random.randint(1, 10**10), random.randint(1, 10**10)
        big_numbers = random.randint(1, 10**100), random.randint(1, 10**100)

        for divisor, dividend in (small_numbers, medium_numbers, big_numbers):
            code, json_result = division(str(divisor), str(dividend))

            self.assertEqual(code, 200)

            result = json.loads(json_result)

            self.assertAlmostEqual(result['quotient'],
                                   float(dividend) / divisor)
            self.assertEqual(result['result'], dividend // divisor)
            self.assertEqual(result['modulus'], dividend % divisor)

    def test_zero_divisor_input(self):
        code, json_result = division(0, 42)

        self.assertEqual(code, 400)
        result = json.loads(json_result)
        self.assertEqual(result['error'], 'Divisor can not be equal to zero.')

    def test_bad_type_input(self):
        bad_inputs = [
            ('23.45', '42'),
            ('"} AND DROP TABLE students;', '42'),
            ('42', 'qwerty'),
            ('42', None),
        ]

        for divisor, dividend in bad_inputs:
            code, json_result = division(divisor, dividend)

            self.assertEqual(code, 400)

            result = json.loads(json_result)

            self.assertEqual(code, 400)
            result = json.loads(json_result)
            self.assertEqual(result['error'],
                             'Both arguments must be base 10 integers.')


if __name__ == '__main__':
    unittest.main()
