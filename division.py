import json


def division(divisor, dividend):
    """Division logic.

    division(divisor, dividend) -> status, json_result

    divisor and dividend (str) - values from an http request
    status (int) - HTTP code
    json_result (str) - result of the operation represented in json format
    """

    try:
        divisor = int(divisor)
        dividend = int(dividend)
    except (ValueError, TypeError):
        return 400, json.dumps({
           "error": "Both arguments must be base 10 integers."
        })

    try:
        quotient = float(dividend) / divisor
        result = dividend // divisor
        modulus = dividend % divisor
    except ZeroDivisionError:
        return 400, json.dumps({
            "error": "Divisor can not be equal to zero."
        })

    return 200, json.dumps({
        "quotient": quotient,
        "result" : result,
        "modulus": modulus
    })
