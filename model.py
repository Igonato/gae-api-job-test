from google.appengine.ext import db


class DivisionResult(db.Model):
    dividend = db.StringProperty(required=True)
    divisor = db.StringProperty(required=True)
    json_result = db.StringProperty(required=True)
    code = db.IntegerProperty(required=True)


def get_saved_result(divisor, dividend):
    q = db.Query(DivisionResult)
    q.filter('divisor =', divisor)
    q.filter('dividend =', dividend)
    return q.get()
